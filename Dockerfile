FROM debian:jessie

RUN apt-get update && apt-get install -yy texlive-pictures bash pdftk poppler-utils
COPY ./ /app
CMD ["sh", "-c","/app/scripts/flyer-pdf-merge"]

