#!/bin/bash
check=`pdffonts $1 | awk '{ if ($4 == "no") print $1; }'`
out=`echo "$check"|sort -u`
if [ "$check" != "" ]
	then
	>&2 echo "FEHLER: nicht alle PDF-Schriften scheinen eingebettet zu sein!";
	>&2 echo $out;
	exit 1;
fi;
echo "fonts ok";

