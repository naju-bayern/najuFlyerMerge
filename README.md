[![build status](https://gitlab.com/naju-bayern/najuFlyerMerge/badges/master/build.svg)](https://gitlab.com/naju-bayern/najuFlyerMerge/commits/master)

# najuFlyerMerge

An app to change pdf's to the required format for printing.

## Usage
    docker run -v <input/dir>:/in -v <output/dir>:/out registry.gitlab.com/naju-bayern/najuflyermerge

input/dir contains the page files in a numbered order (001.pdf,002.pdf,...), a file named _name_, determining the name of the output pdf and a file named _type_ containing one of the following words.

    umweltdruckerei-8pages-wrapped
    udruckerei-FalzFlyer10SeitenGewickelt
    
